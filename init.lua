
-- The public xray
xray = {
    version = "1.0-rel", -- Xray's version
    game = "???", -- Game running (Not really important, as 100% of the code just takes current stuff and overloads with a new node)
    settings = { -- Settings (As defined by minetest.conf, parsed by settings.lua)
        refresh = {
            normal = 3.0,
            fast = 1.0,
        },
        range = {
            normal = 6,
            long = 8,
        },
        invisible_light = 4,
        full_invisible = true
    },
}

_xray = {
    idx = 0, -- Track the next id for the xray:node_#
    cache = { -- Cache of nodes and player data
        players = {
            --[[
            ApolloX939 = {
                enabled = true,
                hud = 13,
                nodes = {
                    "default:stone",
                    "default:cobblestone",
                    "default:desert_stone",
                    "defualt:desert_cobblestone",
                },
                track = {
                    {
                        pos = vector,
                        was = "default:stone",
                        now = "xray:node_#",
                    },
                    {
                        pos = vector,
                        was = "default:cobblestone",
                        now = "xray:node_#",
                    },
                }
            },
            ]]
        },
        nodes = {
            --[[
                "default:stone" = "xray:node_#",
            ]]
        },
    },
    store = minetest.get_mod_storage(), -- Mod storage for player's node list to be saved here
    log = function(msg) -- Logging
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("action", "[xray] " .. tostring(msg))
    end,
    dofile = function(dir, file) -- Run another file
        local modpath = minetest.get_modpath("xray")
        if file == nil then
            dofile(modpath .. DIR_DELIM .. dir .. ".lua")
        else
            dofile(modpath .. DIR_DELIM .. dir .. DIR_DELIM .. file .. ".lua")
        end
    end,
}

-- PS: My mod isn't game dependent! \o/ (This is just for logging)
if minetest.registered_nodes["default:stone"] ~= nil then
    xray.game = "mtg"
elseif minetest.registered_nodes["mcl_core:stone"] ~= nil then
    xray.game = "mcl"
elseif minetest.registered_nodes["nc_terrain:stone"] ~= nil then
    xray.game = "nc"
end

-- Add this to the global table... this gets the length in case it's a map, not a list
-- https://stackoverflow.com/questions/2705793/how-to-get-number-of-entries-in-a-lua-table#2705804
-- TLDR: #table only counts lists, #table does undefined for maps (Which for my case cache.nodes is a map)
table.length = function(T)
    local count = 0
    for _ in pairs(T) do count = count + 1 end
    return count      
end

-- Intro in logs
_xray.log("= Xray ================================")
_xray.log("Version:  " .. xray.version)
_xray.log("Gamemode: " .. xray.game)
_xray.log("          " .. _VERSION) -- Lua Version
_xray.log("========================================")

-- Hyjack the Minetest register_node function so we can catch stuff after the mod's loaded
_xray.origin_node = minetest.register_node
_xray.log("'minetest.register_node' hyjacked!")

_xray.dofile("settings") -- Load/Parse settings from minetest.conf

_xray.dofile("api") -- The API (Give access to cloak and uncloak around a player)
_xray.dofile("abm") -- Auto cleanup tool (Over time, nodes will clean up... unless the node doesn't exist... see Panic!)
_xray.dofile("node") -- Makes the invisible node (So that's what an invisible barrier looks like)

-- The new Hyjacked register_node function
minetest.register_node = function(itemstring, def)
    _xray.register_node(itemstring, def, _xray.idx)
    _xray.idx = _xray.idx + 1
    _xray.origin_node(":"..itemstring, def)
end

_xray.dofile("chat") -- /xray /x and the chat commands
_xray.dofile("events") -- Join, Leave events (Possibly need to add On Server Shutdown for Singleplayer worlds)
_xray.dofile("globalstep") -- Loop over players, do something in realtime here
_xray.dofile("privs") -- Check your Privledge!

_xray.dofile("final") -- Process nodes before this mod
