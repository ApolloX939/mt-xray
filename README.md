# Xray

- [Chat Commands](#chat-commands)
- [Settings & Privs](#settings--privs)
- [How it works](#how-it-works)

## Chat commands

### `/x`

Toggles enable/disable state of xray

### `/xray`

Has 3 sub commands, and defaults to `list`

#### `/xray list`

Lists all nodes that will be converted in range

#### `/xray add`

Can be given either a itemstring for a node, or a special keyword 'look' to use upto 3 blocks away the node being looked at

Adds that node to the list for the current player

#### `/xray rm`

Can be given either a itemstring for a node, or a special keyword 'look' to use upto 3 blocks away the node being looked at

Removes that node from the list for the current player

## Settings & Privs

There are 4 privledges:

- `xray` (Grants access to `/x`, and `/xray` chatcommands)
- `xray_range_long` (Grants additional range for nodes to be converted)
- `xray_fast_refresh` (Grants quicker conversion rate)
- `xray_instant_refresh` (Grants instant conversions with no delay) \*

\* May impact server performance

And complementary settings:

- `xray_range_normal` int default 6 (min: 3, max: 16), range players with just `xray` have
- `xray_range_long` int default 8 (min: 3, max: 16), range players with `xray` and `xray_range_long` have
- `xray_refresh_normal` float default 3.0 (min: 0.0, max: 60.0), time till the next conversion (with only `xray`)
- `xray_refresh_fast` float default 1.0 (min: 0.0, max: 60.0), time till next conversion (with `xray` and `xray_fast_refresh`)
- `xray_light_level` int default 4 (min: 0, max: 14), light level that invisible blocks emit, set to 0 to disable this feature
- `xray_full_invisible` bool default true (true/false), When true, a 100% empty texture is used, while false, a outline is provided

## How it works

My version of Xray contained within [OreTracker](https://content.minetest.net/packages/ApolloX/oretracker/) was quite primative.

> It didn't allow individual players to have their own list of nodes to convert, and that list of nodes it would convert were hardcoded into the mod.

This version is for the record, one of the most powerful (server-side) xray mods.

It forms a list of all nodes and makes invisible counter parts (each node will have an invisible one, with same mining requirements, and drops).

> It does this by parsing all currently loaded nodes (via `minetest.registered_nodes`), then hyjacks/overloads the `minetest.register_node` with it's own.

Once it has a list of given this node, what it's invisible counter node is, it can simply clear any existing invisible nodes, and then find your list of nodes you want invisible, and swap them.

> I use `minetest.swap_node` as it doesn't destroy meta data... so except for maybe nodetimers, it's fairly safe to swap... also I'm only swaping nodes near players, not across the map in possibly unloaded mapblocks.

It provides some chat commands to enable/disable xray and add/remove nodes.

- `/x` to toggle the mod on/off
- `/xray add` or `/xray rm` to add/remove nodes
- `/xray` or `/xray list` to list nodes you currently will be making invisble

It uses a HUD element (currently text with color) to indicate if the mod is on or off (on being green, red being off)

The `/xray add` and `/xray rm` chat commands require an additional parameter, either a itemstring (e.g. `default:stone`), or `look`, which performs a raycast to determine what node you're looking at and adds/removes that node (max range of 3 blocks away).

> When using itemstrings you can space separate the nodes for if you wanted to add/remove multiple nodes at the same time

Each time it scans for new nodes to swap it first swaps the existing invisible nodes, then it scans for nodes to swap.

> 2 things on that. First, yes this is inefficient, it should check if it can swap nodes if not then swap only nodes that are out of range. Second, there is a ABM to clean up nodes, so if the server/client crashes *hopefully* the world should after some time recover.

The way it swaps nodes however does have some side-effects, mainly, if you're on a sever with this mod, you can make use of someone elses xray.

> This isn't fixable for this mod's version, as there *currently* is no way to only make blocks appear invisible to a single client, and as this is a server-side mod, it will affect all clients.
