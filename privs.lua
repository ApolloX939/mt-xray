
minetest.register_privilege("xray", {
    description = "Xray Priv",
    give_to_singleplayer = true,
    give_to_admin = true
})

minetest.register_privilege("xray_range_long", {
    description = "Xray Long Range Priv",
    give_to_singleplayer = false,
    give_to_admin = true
})

minetest.register_privilege("xray_fast_refresh", {
    description = "Xray Fast Refresh Priv",
    give_to_singleplayer = false,
    give_to_admin = false -- Because the admin will have instant
})

minetest.register_privilege("xray_instant_refresh", {
    description = "Xray Instant Refresh Priv",
    give_to_singleplayer = true,
    give_to_admin = true
})
