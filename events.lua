
-- On connect, make the hud element, and prefill cache data
minetest.register_on_joinplayer(function(player, last_login)
    local pname = player:get_player_name()
    _xray.cache.players[pname] = {
        enabled = false,
        delta = 0,
        hud = player:hud_add({
            hud_elem_type = "text",
            position = {x = 0.9, y = 0.9},
            offset = {x = 0.0, y = 0.0},
            text = " xray ",
            number = 0xe10000, -- 225, 0, 0 (RGB) Red
            alignment = {x = 0.0, y = 0.0},
            scale = {x = 100.0, y = 100.0}
        }),
        track = {},
    }
    _xray.get_nodes(pname)
end)

-- On disconnect, if exists in cache, remove it, and revert any nodes
minetest.register_on_leaveplayer(function(player, timed_out)
    local pname = player:get_player_name()
    if _xray.cache.players[pname] ~= nil then
        if _xray.cache.players[pname].enabled == true then
            _xray.cache.players[pname].enabled = false
            --_xray.clear(player)
            _xray.uncloak(player)
        end
        _xray.cache.players[pname] = nil
    end
end)
