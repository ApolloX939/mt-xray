
-- This all makes a xray node with a unique id, which has the same drops and mining rules as the original node
-- Please note, no callbacks are transfered!
_xray.register_node = function(node, def, idx)
    if not node or not def then
        return
    end
    local itemstring = "xray:node_" .. tostring(idx)
    if def.groups and def.groups.oddly_breakable_by_hand ~= 1 then
        def.groups.oddly_breakable_by_hand = 1
    end
    local tex = "xray_clear.png" -- Show an outline or go full invisible
    if xray.settings.full_invisible then
        tex = "xray_invisible.png"
    end
    _xray.origin_node(":"..itemstring, {
        short_description = def.short_description or "Xray Node",
        description = def.description or "Xray Node",
        tiles = {tex},
        groups = def.groups or {oddly_breakable_by_hand = 1, handy = 1, not_in_creative_inventory = 1},
        drop = def.drop or node,
        drawtype = "glasslike",
        sunlight_propagates = true,
        legacy_mineral = true,
        sounds = def.sounds or nil,
        light_source = xray.settings.invisible_light or 4,
        _doc_items_hidden = true,
        stack_max = def.stack_max or 99,
        _mcl_blast_resistance = def._mcl_blast_resistance or nil,
        _mcl_hardness = def._mcl_hardness or nil,
        _mcl_silk_touch_drop = false,
        _mcl_fortune_drop = def._mcl_fortune_drop or nil,
        after_dig_node = function(pos, oldnode, oldmetadata, digger) -- Removes it from the track list so it doesn't get duped
            if _xray.cache.players[digger] ~= nil and _xray.cache.players[digger].track ~= nil and #_xray.cache.players[digger].track ~= 0 then
                local idx = -1
                for i, track in ipairs(_xray.cache.players[digger].track) do
                    if track.pos == pos then
                        idx = i
                    end
                end
                if idx ~= -1 then
                    table.remove(_xray.cache.players[digger].track, idx)
                end
            end
        end,
    })
    _xray.register_abm(itemstring, node) -- Register the cleanup ABM
    _xray.cache.nodes[node] = itemstring -- Store it in the cached nodelist
    _xray.log("\"" .. node .. "\" -> \"" .. itemstring .. "\"") -- Debug log
end
