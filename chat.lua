
-- Position based on where our player is actually looking
local function get_eye_pos(player)
    local pos = player:get_pos()
    pos.y = pos.y + player:get_properties().eye_height
    return pos
end

local rm = function(name, param)
    local player = minetest.get_player_by_name(name)
    if player == nil then
        return false, "[xray] You must be online to use \"/xray rm\" chat command"
    end
    local pos = get_eye_pos(player)
    local dir = player:get_look_dir()
    if _xray.cache.players[name] == nil then
        return false, "[xray] Invalid player state, report this error \"/xray rm node chat command failed to get my player data\""
    end
    local words = param:split(" ")
    if #words == 0 then
        return false, "Use me like this: \"/xray rm mod:node_name\""
    elseif #words == 1 then
        if words[1] == "look" then
            _xray.log("ray start="..minetest.pos_to_string(pos)..", end="..minetest.pos_to_string(pos + dir * 4))
            local ray = minetest.raycast(pos, pos + dir * 4, true, false)
            local thing = nil
            local vec_zero = vector.zero()
            for ithing in ray do
                if ithing.type == "node" then
                    node = minetest.get_node_or_nil(ithing.under)
                    if node then
                        _xray.log("ray '" .. node.name .. "' at " .. minetest.pos_to_string(ithing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")")
                    end
                    thing = ithing
                    break
                end
            end
            if not thing or thing.type ~= "node" or thing.intersection_normal == vec_zero then
                if thing and thing.type == "node" then
                    local node = minetest.get_node_or_nil(thing.under)
                    if node then
                        _xray.log("Raycast hit '" .. node.name .. "' at " .. minetest.pos_to_string(thing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")")
                        return false, "Raycast hit '" .. node.name .. "' at " .. minetest.pos_to_string(thing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")"
                    end
                end
                return false, "[xray] Invalid looking node, report this error \"/xray rm node chat command faild to do the raycast of what I was looking at\""
            end
            local node = minetest.get_node_or_nil(thing.under)
            if node == nil then
                return false, "[xray] Failed obtaining node, report this error \"/xray rm node chat command failed to get node I was looking at\""
            end
            if node.name:sub(1, 4) == "xray" then
                return false, "[xray] Invalid looking node, that node is invisible"
            end
            local nodelist = _xray.get_nodes(name)
            local idx = -1
            for i, k in ipairs(nodelist or {}) do
                if k == node.name then
                    idx = i
                end
            end
            if idx == -1 then
                return false, "Node \""..node.name.."\" not in xray list"
            end
            _xray.rm_node(name, node.name)
            return true, "Node \"".. node.name .. "\" removed"
        else
            local nodelist = _xray.get_nodes(name)
            local idx = -1
            for i, k in ipairs(nodelist or {}) do
                if k == words[1] then
                    idx = i
                end
            end
            if idx == -1 then
                return false, "Node \"".. words[1].."\" not in xray list"
            end
            _xray.rm_node(name, words[1])
            return true, "Node \"".. words[1] .. "\" removed"
        end
    else
        local nodelist = _xray.get_nodes(name)
        for _, w in ipairs(words) do
            local idx = -1
            for i, k in ipairs(nodelist or {}) do
                if k == w then
                    idx = i
                end
            end
            if idx == -1 then
                minetest.chat_send_player(name, "Node \""..w.."\" not in xray list")
            else
                _xray.rm_node(name, w)
                minetest.chat_send_player(name, "Node \""..w.."\" removed")
            end
        end
    end
end

local add = function(name, param)
    local player = minetest.get_player_by_name(name)
    if player == nil then
        return false, "[xray] You must be online to use \"/xray add\" chat command"
    end
    local pos = get_eye_pos(player)
    local dir = player:get_look_dir()
    if _xray.cache.players[name] == nil then
        return false, "[xray] Invalid player state, report this error \"/xray add node chat command failed to get my player data\""
    end
    local words = param:split(" ")
    if #words == 0 then
        return false, "Use me like this: \"/xray add mod:node_name\""
    elseif #words == 1 then
        if words[1] == "look" then
            _xray.log("ray start="..minetest.pos_to_string(pos)..", end="..minetest.pos_to_string(pos + dir * 4))
            local ray = minetest.raycast(pos, pos + dir * 4, true, false)
            local thing = nil
            local vec_zero = vector.zero()
            local node = nil
            for ithing in ray do
                if ithing.type == "node" then
                    node = minetest.get_node_or_nil(ithing.under)
                    if node then
                        _xray.log("ray '" .. node.name .. "' at " .. minetest.pos_to_string(ithing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")")
                    end
                    thing = ithing
                    break
                end
            end
            if not thing or thing.type ~= "node" or thing.intersection_normal == vec_zero then
                if thing and thing.type == "node" then
                    local node = minetest.get_node_or_nil(thing.under)
                    if node then
                        _xray.log("Raycast hit '" .. node.name .. "' at " .. minetest.pos_to_string(thing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")")
                        return false, "Raycast hit '" .. node.name .. "' at " .. minetest.pos_to_string(thing.under) .. " (pos " .. minetest.pos_to_string(pos) .. ")"
                    end
                end
                return false, "[xray] Invalid looking node, report this error \"/xray add node chat command faild to do the raycast of what I was looking at\""
            end
            node = minetest.get_node_or_nil(thing.under)
            if node == nil then
                return false, "[xray] Failed obtaining node, report this error \"/xray add node chat command failed to get node I was looking at\""
            end
            if node.name:sub(1, 4) == "xray" then
                return false, "[xray] Invalid looking node, that node is invisible"
            end
            local nodelist = _xray.get_nodes(name)
            for _, k in ipairs(nodelist or {}) do
                if k == node.name then
                    return false, "Node \""..node.name.."\" already in xray list"
                end
            end
            _xray.add_node(name, node.name)
            return true, "Node \"".. node.name .. "\" added"
        else
            local nodelist = _xray.get_nodes(name)
            for _, k in ipairs(nodelist or {}) do
                if k == words[1] then
                    return false, "Node \"".. words[1].."\" already in xray list"
                end
            end
            if minetest.registered_nodes[words[1]] == nil then
                return false, "Invalid node name"
            end
            _xray.add_node(name, words[1])
            return true, "Node \"".. words[1] .. "\" added"
        end
    else
        local nodelist = _xray.get_nodes(name)
        for _, w in ipairs(words) do
            local con = true
            for _, k in ipairs(nodelist or {}) do
                if k == w then
                    minetest.chat_send_player(name, "Node \""..w.."\" already in xray list")
                    con = false
                end
            end
            if con then
                if minetest.registered_nodes[w] == nil then
                    minetest.chat_send_player(name, "Invalid node \""..w.."\" name")
                    con = false
                end
                if con then
                    _xray.add_node(name, w)
                    minetest.chat_send_player(name, "Node \""..w.."\" added")
                end
            end
        end
    end
end

local list = function(name, param)
    local player = minetest.get_player_by_name(name)
    if player == nil then
        return false, "[xray] You must be online to use \"/xray list\" chat command"
    end
    if _xray.cache.players[name] == nil then
        return false, "[xray] Invalid player state, report this error \"/xray list chat command failed to get my player data\""
    end
    local nodelist = _xray.get_nodes(name)
    if #nodelist == 0 then
        return true, "No nodes in xray list"
    end
    minetest.chat_send_player(name, "Xray Node List: (" .. #nodelist .. ")")
    for _, n in ipairs(nodelist) do
        minetest.chat_send_player(name, "  \"" .. n .. "\"")
    end
    return true
end

minetest.register_chatcommand("x", {
    privs = {
        xray = true,
    },
    
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if player == nil then
            return false, "[xray] You must be online to use \"/x\" chat command"
        end
        if _xray.cache.players[name] == nil then
            return false, "[xray] Invalid player state, report this error \"/x chat command failed to get my player data\""
        end
        _xray.cache.players[name].enabled = not _xray.cache.players[name].enabled
        if _xray.cache.players[name].enabled then
            _xray.cloak(player)
            if  _xray.cache.players[name].hud == nil then
                _xray.cache.players[name].hud = player:hud_add({
                    hud_elem_type = "text",
                    position = {x = 0.9, y = 0.9},
                    offset = {x = 0.0, y = 0.0},
                    text = " xray ",
                    number = 0x00e100, -- 0, 225, 0 (RGB) Green
                    alignment = {x = 0.0, y = 0.0},
                    scale = {x = 100.0, y = 100.0}
                })
            else
                player:hud_change(_xray.cache.players[name].hud, "number", 0x00e100)
            end
        else
            _xray.uncloak(player)
            if  _xray.cache.players[name].hud == nil then
                _xray.cache.players[name].hud = player:hud_add({
                    hud_elem_type = "text",
                    position = {x = 0.9, y = 0.9},
                    offset = {x = 0.0, y = 0.0},
                    text = " xray ",
                    number = 0xe10000, -- 225, 0, 0 (RGB) Red
                    alignment = {x = 0.0, y = 0.0},
                    scale = {x = 100.0, y = 100.0}
                })
            else
                player:hud_change(_xray.cache.players[name].hud, "number", 0xe10000)
            end
        end
        return true
    end,
})

minetest.register_chatcommand("xray", {
    privs = {
        xray = true,
    },
    func = function(name, param)
        local words = param:split(" ")
        local args = ""
        if #words >= 2 then
            for i, w in ipairs(words) do
                if i >= 2 and w then
                    args = args .. " " .. w
                end
            end
        end
        if #words == 0 or words[1] == "list" then
            return list(name, args)
        elseif words[1] == "add" then
            return add(name, args)
        elseif words[1] == "rm" then
            return rm(name, args)
        end
    end,
})

minetest.register_chatcommand("xray list", {
    privs = {
        xray = true,
    },
    func = list,
})

minetest.register_chatcommand("xray add", {
    privs = {
        xray = true,
    },
    func = add,
})

minetest.register_chatcommand("xray rm", {
    privs = {
        xray = true,
    },
    func = rm,
})
