
_xray.log("Pre-Loading ".. tostring(table.length(minetest.registered_nodes)) .. " nodes...")

-- Process previous nodes that have already been defined
for n, def in pairs(minetest.registered_nodes) do
    if n and def then
        if n:sub(1, 4) ~= "xray" and n ~= "air" and n ~= "ignore" then
            _xray.register_node(n, def, _xray.idx)
            _xray.idx = _xray.idx + 1
        end
    end
end

_xray.log("Pre-Loaded " .. tostring(_xray.idx + 1) .. " nodes")
_xray.log("= READY =")
