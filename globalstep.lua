
minetest.register_globalstep(function(dtime)
    for _, p in ipairs(minetest.get_connected_players()) do
        local pname = p:get_player_name()
        if _xray.cache.players[pname] ~= nil then
            _xray.cache.players[pname].delta = _xray.cache.players[pname].delta + dtime
            local max = 0
            if minetest.check_player_privs(pname, {xray_fast_refresh=true}) then
                max = xray.settings.refresh.fast or 1.0
            else
                max = xray.settings.refresh.normal or 3.0
            end
            if _xray.cache.players[pname].delta >= max or minetest.check_player_privs(pname, {xray_instant_refresh=true}) then
                _xray.cache.players[pname].delta = 0.0
                if _xray.cache.players[pname].enabled == true then
                    _xray.cloak(p)
                end
            end
        end
    end
end)
