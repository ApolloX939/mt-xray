
_xray.register_abm = function(from, to)
    minetest.register_abm({
        nodenames = {from},
        interval = 0,
        action = function(pos, node, active_object_count, active_object_count_wider)
            minetest.swap_node(pos, {name=to})
        end,
    })
end
