
-- Refresh
xray.settings.refresh.normal = minetest.settings:get("xray_refresh_normal")
if xray.settings.refresh.normal == nil then
    xray.settings.refresh.normal = 3.0
    minetest.settings:set("xray_refresh_normal", 3.0)
else
    xray.settings.refresh.normal = tonumber(xray.settings.refresh.normal)
    if xray.settings.refresh.normal < 0.0 then
        xray.settings.refresh.normal = 0.0
    end
    if xray.settings.refresh.normal > 60.0 then
        xray.settings.refresh.normal = 60.0
    end
end

xray.settings.refresh.fast = minetest.settings:get("xray_refresh_fast")
if xray.settings.refresh.fast == nil then
    xray.settings.refresh.fast = 1.0
    minetest.settings:set("xray_refresh_fast", 1.0)
else
    xray.settings.refresh.fast = tonumber(xray.settings.refresh.fast)
    if xray.settings.refresh.fast < 0.0 then
        xray.settings.refresh.fast = 0.0
    end
    if xray.settings.refresh.fast > 60.0 then
        xray.settings.refresh.fast = 60.0
    end
end

-- Range
xray.settings.range.normal = minetest.settings:get("xray_range_normal")
if xray.settings.range.normal == nil then
    xray.settings.range.normal = 6
    minetest.settings:set("xray_range_normal", 6)
else
    xray.settings.range.normal = tonumber(xray.settings.range.normal)
    if xray.settings.range.normal < 3 then
        xray.settings.range.normal = 3
    end
    if xray.settings.range.normal > 16 then
        xray.settings.range.normal = 16
    end
end

xray.settings.range.long = minetest.settings:get("xray_range_long")
if xray.settings.range.long == nil then
    xray.settings.range.long = 8
    minetest.settings:set("xray_range_long", 8)
else
    xray.settings.range.long = tonumber(xray.settings.range.long)
    if xray.settings.range.long < 3 then
        xray.settings.range.long = 3
    end
    if xray.settings.range.long > 16 then
        xray.settings.range.long = 16
    end
end

-- Lighting
xray.settings.invisible_light = minetest.settings:get("xray_light_level")
if xray.settings.invisible_light == nil then
    xray.settings.invisible_light = 4
    minetest.settings:set("xray_light_level", 4)
else
    xray.settings.invisible_light = tonumber(xray.settings.invisible_light)
    if xray.settings.invisible_light < 0 then
        xray.settings.invisible_light = 0
    end
    if xray.settings.invisible_light > 14 then
        xray.settings.invisible_light = 14
    end
end

-- Visibility
xray.settings.full_invisible = minetest.settings:get_bool("xray_full_invisible", true)

_xray.log("= Settings =")
_xray.log("'xray_refresh_normal' = '" .. tostring(xray.settings.refresh.normal) .. "'")
_xray.log("'xray_refresh_fast' = '" .. tostring(xray.settings.refresh.fast) .. "'")
_xray.log("'xray_range_normal' = '" .. tostring(xray.settings.range.normal) .. "'")
_xray.log("'xray_range_long' = '" .. tostring(xray.settings.range.long) .. "'")
_xray.log("'xray_light_level' = '" .. tostring(xray.settings.invisible_light) .. "'")
_xray.log("'xray_full_invisible' = '" .. tostring(xray.settings.full_invisible) .. "'")
