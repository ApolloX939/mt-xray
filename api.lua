
_xray.cloak = function(player)
    if player == nil then
        return false
    end
    local pname = player:get_player_name()
    if _xray.cache.players[pname] == nil then
        return false
    end
    _xray.uncloak(player) -- Clear existing nodes first!
    local pos = player:get_pos()
    local nodelist = _xray.get_nodes(pname)
    local range_vec = vector.new(0, 0, 0)
    -- Dynamic range based on extra privs
    if minetest.check_player_privs(pname, {xray_range_long=true}) then
        range_vec.x = xray.settings.range.long or 8
        range_vec.y = xray.settings.range.long or 8
        range_vec.z = xray.settings.range.long or 8
    else
        range_vec.x = xray.settings.range.normal or 6
        range_vec.y = xray.settings.range.normal or 6
        range_vec.z = xray.settings.range.normal or 6
    end
    local pos1 = vector.subtract(pos, range_vec)
    local pos2 = vector.add(pos, range_vec)
    local hits = minetest.find_nodes_in_area(pos1, pos2, nodelist or {})
    for i=1, #hits do
        local delta = vector.subtract(hits[i], pos)
        local distance = delta.x*delta.x + delta.y*delta.y + delta.z*delta.z
        if distance <= range_vec.x*range_vec.x then
            local node = minetest.get_node_or_nil(hits[i])
            if node then
                local swap = _xray.cache.nodes[node.name] or nil
                if swap then
                    --_xray.log("C \"" .. node.name .. "\" > \"" .. swap .. "\"")
                    table.insert(_xray.cache.players[pname].track, {
                        pos = hits[i],
                        was = node.name,
                        now = swap,
                    })
                    minetest.swap_node(hits[i], {name = swap})
                end
            end
        end
    end
    return true
end

--_xray.clear = function(player)
_xray.uncloak = function(player)
    if player == nil then
        return false
    end
    local pname = player:get_player_name()
    if _xray.cache.players[pname] == nil then
        return true
    end
    local list = _xray.cache.players[pname].track or {}
    for _, track in ipairs(list) do
        local node = minetest.get_node_or_nil(track.pos)
        if node and node.name == track.now then -- Only clear nodes we've converted, don't clear nodes that don't exist anymore
            minetest.swap_node(track.pos, {name =track.was})
        end
    end
    -- Because we run thru EVERY node, we can safely remove ALL
    _xray.cache.players[pname].track = {}
end

-- It appears this doesn't work for anything, so instead we'll just use clear instead
--[[_xray.uncloak = function(player)
    if player == nil then
        return false
    end
    local pname = player:get_player_name()
    if _xray.cache.players[pname] == nil then
        return true
    end
    _xray.clear(player)
    local nodelist = _xray.get_nodes(pname)
    local tracklist = _xray.cache.players[pname].track or {}
    local pos = player:get_pos()
    local range_vec = vector.new(0, 0, 0)
    if minetest.check_player_privs(pname, {xray_range_long=true}) then
        range_vec.x = xray.settings.range.long or 8
        range_vec.y = xray.settings.range.long or 8
        range_vec.z = xray.settings.range.long or 8
    elseif minetest.check_player_privs(pname, {xray_range_normal=true}) then
        range_vec.x = xray.settings.range.normal or 6
        range_vec.y = xray.settings.range.normal or 6
        range_vec.z = xray.settings.range.normal or 6
    end
    local pos1 = vector.subtract(pos, range_vec)
    local pos2 = vector.add(pos, range_vec)
    local swap_list = {}
    local node_list = {}
    for _, v2 in ipairs(nodelist) do
        for k1, v1 in pairs(_xray.cache.nodes) do
            if v2 == k1 then
                table.insert(node_list, v1)
                swap_list[v1] = k1
            end
        end
    end
    --_xray.log("node_list = " .. minetest.serialize(node_list))
    --_xray.log("swap_list = " .. minetest.serialize(swap_list))
    local hits = minetest.find_nodes_in_area(pos1, pos2, node_list)
    --_xray.log("hits " .. tostring(#hits) .. " = " .. minetest.serialize(hits))
    for i=1, #hits do
        local delta = vector.subtract(hits[i], pos)
        local distance = delta.x*delta.x + delta.y*delta.y + delta.z*delta.z
        if distance <= range_vec.x*range_vec.x then
            local node = minetest.get_node_or_nil(hit[i])
            local idx = -1
            for i, t in ipairs(tracklist) do
                if t.pos == hit[i] then
                    idx = i
                end
            end
            if idx ~= -1 then
                table.remove(_xray.cache.players[pname].track, idx)
                table.remove(tracklist, idx)
            end
            if node then
                local swap = swap_list[node.name] or nil
                if swap then
                    _xray.log("U \"" .. node.name .. "\" > \"" .. swap .. "\"")
                    minetest.swap_node(hits[i], {name = swap})
                end
            end
        end
    end
    return true
end]]

_xray.get_nodes = function (name)
    local lists = minetest.deserialize(_xray.store:get_string("xray_player_nodelists") or "") or {}
    if lists[name] == nil then
        lists[name] = {}
        _xray.store:set_string("xray_player_nodelists", minetest.serialize(lists))
        return {}
    else
        return lists[name]
    end
end

_xray.add_node = function (name, itemstring)
    if itemstring:sub(1, 4) == "xray" then
        return nil
    end
    local lists = minetest.deserialize(_xray.store:get_string("xray_player_nodelists") or "") or {}
    if lists[name] == nil then
        lists[name] = { itemstring }
        _xray.store:set_string("xray_player_nodelists", minetest.serialize(lists))
    else
        table.insert(lists[name], itemstring)
        _xray.store:set_string("xray_player_nodelists", minetest.serialize(lists))
    end
end

_xray.rm_node = function (name, itemstring)
    if itemstring:sub(1, 4) == "xray" then
        return nil
    end
    local lists = minetest.deserialize(_xray.store:get_string("xray_player_nodelists") or "") or {}
    if lists[name] == nil then
        lists[name] = {}
        _xray.store:set_string("xray_player_nodelists", minetest.serialize(lists))
    else
        local idx = -1
        for i, n in ipairs(lists[name]) do
            if n == itemstring then
                idx = i
            end
        end
        if idx ~= -1 then
            table.remove(lists[name], idx)
            _xray.store:set_string("xray_player_nodelists", minetest.serialize(lists))
        end
    end
end
